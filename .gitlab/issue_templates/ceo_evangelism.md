* [ ] The technical evangelism team creates an issue in the Corporate Marketing project [using the label of `sid-evangelism`](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues?label_name%5B%5D=sid-evangelism)
* [ ] Where there is content alignment, the technical evangelism team provides the thesis of the talk along with an outline in slide deck form
* [ ] The DRI collaborates on this thesis and outline and then presents it to the Exec for approval
* [ ] The DRI takes over slide deck creation with design support from our PR firm as necessary (one week lead time needed to acquire their resources)

## Slide Deck Review

* [ ] Slides have talk track `So glad to be with you all here today`
* [ ] Slides have summary at top `A --> B `
* [ ] Visuals make sense and are not duplicated
